var lib = {
    initNav: function () {
        WebService.WS_getScenarios(lib.onScenariosReady);

    },
    onScenariosReady: function (scenarios) {
        //prepare scenarios as dropdown data
        let scenArr = [];
        for (let i = 0; i < scenarios.length; ++i) {
            let tracksArr = [];

            scenArr.push({
                text: scenarios[i].name,
                onClick: function loadScenario() {
                    $("loader").className = ""; //show loading screen
                    WebService.WS_onSimulationReady(lib.onSimulationReady);
                    //request scenario
                    WebService.WS_startSimulationWithScenario(scenarios[i].id, NUM_MAP_SECTOR, lib.onSimulationFinished);
                },
            });
        }

        //update controls
        NavBar([
            {
                text: USER.name,
                clas: 'fa fa-user-circle'
            },
            {
                text: 'Scenarios',
                clas: 'fa fa-file-text',
                items: scenArr
            },
            {
                text: 'Upload Map',
                clas: 'fa fa-upload',
                id: 'uploadMap', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadMap");
                    let upload = link.children[link.children.length - 1];
                    upload.click();
                }
            },
            {
                text: 'Upload Scenario',
                clas: 'fa fa-upload',
                id: 'uploadScenario', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadScenario");
                    let upload = link.children[link.children.length - 1];
                    upload.click();
                }
            },

            {
                text: 'Upload Car file',
                clas: 'fa fa-upload',
                id: 'uploadCarfile', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadCarfile");
                    let upload = link.children[link.children.length-1];
                    upload.click();
                }
            },

            {
                text: 'Logout',
                clas: 'fa fa-window-close',
                onClick: function logout() {
                    WebService.WS_logout(lib.initNav);
                }
            }
        ], document.getElementById("nav-menu"), true).then(function onNavBarReady() {
            //create upload input after NavBar is ready
            let uploadMap = document.createElement("input");
            uploadMap.setAttribute("type", "file");
            uploadMap.setAttribute("multiple", "multiple");

            uploadMap.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadMap(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }
            //add to the uplaod link
            document.getElementById("uploadMap").appendChild(uploadMap);

            // //create upload scenario form
            let uploadScenario = document.createElement("input");
            uploadScenario.setAttribute("type", "file");
            uploadScenario.setAttribute("multiple", "multiple");

            uploadScenario.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadScenario(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }

            //add to the uplaod link
            document.getElementById("uploadScenario").appendChild(uploadScenario);

            //create upload car file form
            let uploadCarfile = document.createElement("input");
            uploadCarfile.setAttribute("type", "file");
            uploadCarfile.setAttribute("multiple", "multiple");

            uploadCarfile.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadCarfile(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }
            //add to the uplaod link
            document.getElementById("uploadCarfile").appendChild(uploadCarfile);



        });

    },


    onStopClicked: function () {
        //stop simulation
        WebService.WS_stop(function onSectorLeave() {
            //fetch scenarios again and build navigation bar
            WebService.WS_getScenarios(lib.onScenariosReady);

        });
    },
	
	 animate: function () {
      //TODO
      if (DataModel.simulationTimeDeltaMs > 0) {
          setTimeout(function() {requestAnimationFrame(lib.animate);}, DataModel.simulationTimeDeltaMs);
      } else {
          requestAnimationFrame( lib.animate );
      }
	},

    onSimulationFinished: function (simulationId) {
        //start animation after the environment is ready is processed
        //attach DataModel to WebSocket connection
        WebService.WS_attachListener(DataModel.onData);
        for (var i = 0; i < NUM_MAP_SECTOR; i++) {
            DataModel.onDataProcessed = lib.animate;
            WebService.WS_loadMapSector(simulationId, i, lib.onMapLoaded)
        }
    },

    onMapLoaded: function (mapData) {
        //build world
        DataModel.onData(mapData);
    },


    onSimulationReady: function (duration) {
        //hide loading screen
        $("loader").className = "disabled";
        //show message
        let sec = Math.round(duration / 1000);
        let min = Math.floor(sec / 60);
        sec -= 60 * min;

        let message = (min > 0 ? min + "m " : "") + sec + "s.";

        $("message").innerHTML = "Simulation done for " + message + " Press play to start/resume simulation.";
        $("message").className = "";

        console.log(message);


        //update controls
        NavBar([
            {
                text: USER.name,
                clas: 'fa fa-user-circle'
            },
            {
                text: 'Play',
                clas: 'fa fa-play',
                onClick: function play() {
                    //disable orbit controls
                    if(!mapbox) controls.enabled = false;
                    $("message").className = "disabled";
                    WebService.WS_onUserUpdate(function updateUser(e) {
                        USER = e.user;
                    });
                    WebService.WS_start();
                    if(mapbox) lib.initCarButtons();
                }
            },
            {
                text: 'Back',
                clas: 'fa fa-step-backward',
                onClick: lib.onStopClicked
            }
        ], document.getElementById("nav-menu"), true);
    },

	simCars: null,
    initCarButtons: function () {
        simCars = [];
        var createdCarButton = false;

        if (!mapbox && !createdCarButton) {
			for (var car in CARS) {
				simCars.push({
					text: car,
					onClick: function onCarSelected() {
						console.log("Selected car: " + this.text);
						if (!CARS[this.text].root) {
							loader.load(CARS[this.text].url, function (geometry) {
								createScene(geometry, this.text)
							});
						} else {
							switchCar(this.text);
						}

					}
				});
			}
			createdCarButton = true;
        }
        else if (mapbox && !createdCarButton) {
            //var carsLength = Object.keys(CARS).length;
            //console.log(carsLength);
            console.log(carNumber);
            for (var i= 1; i <= carNumber ;i++) {
                simCars.push({
                    text: i,
                    onClick: function onCarSelected() {
                        console.log("Selected car: " + this.text);
                        switchCar(this.text);
                    }
                });
            }
            createdCarButton = true;
        }

        //update controls
        NavBar([
                {
                    text: USER.name,
                    clas: 'fa fa-user-circle'
                },
                {
                    text: 'Vehicles',
                    clas: 'fa fa-car',
                    items: simCars
                },
                {
                    text: 'Pause',
                    clas: 'fa fa-pause',
                    onClick: lib.onPauseClicked
                },
                {
                    text: 'Stop',
                    clas: 'fa fa-stop',
                    onClick: lib.onStopClicked
                },
                {
                    text: 'CarInfo',
                    clas: 'fa fa-info-circle',
                    onClick: infoBox.showInfoBox
                 },
                {
                    text: 'Replay',
                    clas: 'fa fa-step-backward',
                    onClick: function play() {
                        //disable orbit controls
                        if(!mapbox) {                          
                            controls.enabled = false;
                        }
						text.frames = 0;
                        $("message").className = "disabled";
                        WebService.WS_onUserUpdate(function updateUser(e) {
                            USER = e.user;
                        });
                        WebService.WS_start();
                    },
                },


            ],

            document.getElementById("nav-menu"), true);
			
			if(!createdProgressbar){
            const progressbarWrapper = document.createElement('div');
            const progressbar = document.createElement('div');
            const value = document.createElement('div');
            const numValue = document.createElement('div');
            progressbarWrapper.id = 'progWrapper';
            progressbarWrapper.classList.add('progressbarWrapper');
            progressbar.id = 'progress';
            progressbar.classList.add('progressbar');
            value.id = 'val';
            value.classList.add('value');
            numValue.classList.add('numvalue');
            numValue.id = 'numVal';

            text.lf = Server.simulationDataframes.length;
            document.body.appendChild(progressbarWrapper);
            // document.getElementById('progWrapper').appendChild(progressbar);
            // document.getElementById('progress').appendChild(value);
            progressbarWrapper.appendChild(numValue);
            progressbarWrapper.appendChild(progressbar);
            progressbar.appendChild(value);
			createdProgressbar = true;
			}

			if (!mapbox) {              
                update_datcontrols();
            }          
        },
		
		onUNPauseClicked: function () {
			//Pause simulation animations

			if(mapbox){
			   //TODO
			   //WebService.WS_continue();
				WebService.WS_rewind(text.frames);
			}
			else{
				WebService.WS_rewind(text.frames);
			}

			// Update Navbar
			NavBar([
				{
					text: USER.name,
					clas: 'fa fa-user-circle'
				},
				{
					text: 'Vehicles',
					clas: 'fa fa-car',
					items: simCars
				},
				{
					text: 'Pause',
					clas: 'fa fa-pause',
					onClick: lib.onPauseClicked
				},
				{
					text: 'Stop',
					clas: 'fa fa-stop',
					onClick: lib.onStopClicked
				},
				{
					text: 'CarInfo',
					clas: 'fa fa-info-circle',
					onClick: infoBox.showInfoBox
				},
				{
					text: 'Replay',
					clas: 'fa fa-step-backward',
					onClick: function play() {
						//disable orbit controls
						if(!mapbox) {
							controls.enabled = false;
						}
						text.frames = 0;
						$("message").className = "disabled";
						WebService.WS_onUserUpdate(function updateUser(e) {
							USER = e.user;
						});
						WebService.WS_start();
					},
				},
			], document.getElementById("nav-menu"), true);
		},
		
		onPauseClicked: function () {
			//Pause simulation animations
			WebService.WS_pause();
			// Update Navbar
			NavBar([
				{
					text: USER.name,
					clas: 'fa fa-user-circle'
				},
				{
					text: 'Vehicles',
					clas: 'fa fa-car',
					items: simCars
				},
				{
					text: 'Continue',
					clas: 'fa fa-play',
					onClick: lib.onUNPauseClicked
				},
				{
					text: 'Stop',
					clas: 'fa fa-stop',
					onClick: lib.onStopClicked
				},
				{
					text: 'CarInfo',
					clas: 'fa fa-info-circle',
					onClick: infoBox.showInfoBox
				},
				{
					text: 'Replay',
					clas: 'fa fa-step-backward',
					onClick: function play() {
						//disable orbit controls
						if(!mapbox) {
							controls.enabled = false;
						}
						text.frames = 0;						
						$("message").className = "disabled";
						WebService.WS_onUserUpdate(function updateUser(e) {
							USER = e.user;
						});

						WebService.WS_start();
						lib.initCarButtons();
					},
				},
			], document.getElementById("nav-menu"), true);
         },

		updateProgressBar: function (addevent) {
			let width = (text.frames * screenWidth) / text.lf;

			const numericValue = document.getElementById('numVal');
			const progressWidth = document.getElementById('val');
			const bar = document.getElementById('progress');
			numericValue.innerHTML = text.frames + '/' + text.lf;
			progressWidth.style.width = width + 'px';
			if (addevent) {
				bar.addEventListener('click', function (e) {
					let offset = this.getClientRects()[0];
					console.log(offset);
					console.log(e);

					let x = e.clientX - offset.left;

					let ptX = Math.floor((e.clientX / bar.offsetWidth) * text.lf);
					console.log(x);
					console.log(ptX);
					progressWidth.style.width = x + 'px';
					numericValue.innerHTML = ptX + '/' + text.lf;
					WebService.WS_rewind(ptX - 2);
					WebService.WS_nextFrame();
					WebService.WS_nextFrame();

					lib.onPauseClicked();

				})
			}
		}

};