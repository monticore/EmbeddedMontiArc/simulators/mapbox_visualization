'use static';

function MapboxEnvironment(mapCenter){

	var map = new mapboxgl.Map({
		container: 'map',
		style: 'mapbox://styles/ebruca/cjz8v0zmc40nw1co2qjiqmh85',
		zoom: 18,
		center: mapCenter,
		pitch: 0,
		bearing:0,
		antialias: true
	});
	
	return map;
}