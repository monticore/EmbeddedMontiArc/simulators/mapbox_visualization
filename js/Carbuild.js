'use strict';

function setCars() {

    var DEF_CARS = {
        "porsche": {
            name:	"Porsche",
            file: 	"porsche.glb",
            scale: 0.20

        },
        "lfa": {
            name: 	"LFA",
            file:   "lfa.glb",
            scale: 1
        }
    };

    return {
        build: function (id, carType, carPosition, carSteering){

            var carConfig = DEF_CARS[carType];
            var car = Object.assign(new CarObject(), carConfig);
            car.modelScale = carConfig.scale;
            car.type = carType;
            car.modelName = carConfig.name;
            car.id = id;
            car.position = carPosition;
            car.rotation = carSteering;
			car.dataBuffer = null;
			car.targetStatus = {};
			car.speed = 0;
			car.acceleration = 0;

			/*
            // configure the car
            if (carType == 'porsche') {
                this.configPorsche(car);
            }else if (carType == 'lfa') {
                this.configLfa(car);
            }
			*/
            return car;
        },


       /*
         //Configuration of porsche - TODO Werte anpassen
        
        configPorsche: function (porsche) {
            porsche.MAX_SPEED = 400;
            porsche.MAX_REVERSE_SPEED = -600;

            porsche.MAX_WHEEL_ROTATION = 0.4;

            porsche.FRONT_ACCELERATION = 300;
            porsche.BACK_ACCELERATION = 500;

            porsche.WHEEL_ANGULAR_ACCELERATION = 1.6;

            porsche.FRONT_DECCELERATION = 200;
            porsche.WHEEL_ANGULAR_DECCELERATION = 1.0;

            porsche.STEERING_RADIUS_RATIO = 0.0083;
        },


     
         //Configuration of lfa
        
        configLfa: function (lfa) {
            lfa.backWheelOffset = 6.5;

            lfa.MAX_SPEED = 350;
            lfa.MAX_REVERSE_SPEED = -550;

            lfa.MAX_WHEEL_ROTATION = 0.4;

            lfa.FRONT_ACCELERATION = 300;
            lfa.BACK_ACCELERATION = 500;

            lfa.WHEEL_ANGULAR_ACCELERATION = 1.6;

            lfa.FRONT_DECCELERATION = 200;
            lfa.WHEEL_ANGULAR_DECCELERATION = 1.0;

            lfa.STEERING_RADIUS_RATIO = 0.0083;
        }
		*/
    };
}
